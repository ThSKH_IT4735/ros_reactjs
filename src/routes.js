/*!

=========================================================
* Argon Dashboard React - v1.2.3
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-react
* Copyright 2023 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
// import Index from "views/Index.js";
// import Profile from "views/pages/Profile.js";
// import Maps from "views/pages/Maps.js";
// import Tables from "views/pages/Tables.js";
// import Icons from "views/pages/Icons.js";
// import DevicesPage from "views/pages/Devices";
// import HomeManagement from "views/pages/Homes";
import RobotManagement from "views/pages/RobotsPage";
import Register from "views/pages/Register.js";
import Login from "views/pages/Login.js";
import Localization from "views/pages/Localization";
import TaskMonitor from "views/pages/TaskMonitor";
import RobotDetail from "views/pages/RobotsPage/components/RobotDetail";
var routes = [
  // {
  //   path: "/index",
  //   name: "Dashboard",
  //   icon: "ni ni-tv-2 text-primary",
  //   component: <Index />,
  //   layout: "/admin",
  // },
  // {
  //   path: "/devices",
  //   name: "Devices",
  //   icon: "bi bi-hdd-stack",
  //   component: <DevicesPage />,
  //   layout: "/admin",
  // },
  // {
  //   path: "/home-management",
  //   name: "Home",
  //   icon: "bi bi-house",
  //   component: <HomeManagement />,
  //   layout: "/admin",
  // },
  {
    path: "robot-management",
    name: "Robot Management",
    icon: "ni ni-controller",
    childPath: ":robotId",
    childComponent: <RobotDetail />,
    component: <RobotManagement />,
    layout: "/admin",
  },
  {
    path: "task-monitor",
    name: "Task Monitor",
    icon: "ni ni-badge text-blue",
    component: <TaskMonitor />,
    layout: "/admin",
  },
  {
    path: "localization",
    name: "Localization",
    icon: "ni ni-pin-3 text-orange",
    component: <Localization />,
    layout: "/admin",
  },
  // {
  //   path: "/icons",
  //   name: "Icons",
  //   icon: "ni ni-planet text-blue",
  //   component: <Icons />,
  //   layout: "/admin",
  // },
  // {
  //   path: "/maps",
  //   name: "Maps",
  //   icon: "ni ni-pin-3 text-orange",
  //   component: <Maps />,
  //   layout: "/admin",
  // },
  // {
  //   path: "/user-profile",
  //   name: "User Profile",
  //   icon: "ni ni-single-02 text-yellow",
  //   component: <Profile />,
  //   layout: "/admin",
  // },
  // {
  //   path: "/tables",
  //   name: "Tables",
  //   icon: "ni ni-bullet-list-67 text-red",
  //   component: <Tables />,
  //   layout: "/admin",
  // },
  {
    path: "login",
    name: "Login",
    icon: "ni ni-key-25 text-info",
    component: <Login />,
    layout: "/auth",
  },
  {
    path: "register",
    name: "Register",
    icon: "ni ni-circle-08 text-pink",
    component: <Register />,
    layout: "/auth",
  },
];
export default routes;
