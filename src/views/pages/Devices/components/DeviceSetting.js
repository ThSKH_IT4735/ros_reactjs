import React from "react";
// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  FormGroup,
  Form,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Modal,
  Row,
  Col,
  ModalHeader,
  ModalBody,
} from "reactstrap";

const DeviceSetting = (props) => {
  const roomLists = [
    {
      name: "Kitchen",
      id: "kitchen_1",
    },
    {
      name: "Study Room",
      id: "study_room_1",
    },
    {
      name: "Dining Room",
      id: "study_dinning_1",
    },
    {
      name: "Living Room",
      id: "study_living_1",
    },
  ];
  console.log("🚀 ~ file: index.js:20 ~ DeviceSetting ~ props:", props);
  return (
    <Modal
      className="modal-dialog-centered"
      size="sm"
      isOpen={props.isShowModal}
      toggle={() => {
        props.setIsShowModal();
      }}
    >
      <ModalHeader>Modal title</ModalHeader>
      <ModalBody></ModalBody>
    </Modal>
  );
};
export default DeviceSetting;
