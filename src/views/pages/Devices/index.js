/*!

=========================================================
* Argon Dashboard React - v1.2.3
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-react
* Copyright 2023 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import { useState } from "react";
// react component that copies the given text inside your clipboard
import { CopyToClipboard } from "react-copy-to-clipboard";
// reactstrap components
import {
  Card,
  CardHeader,
  CardBody,
  Container,
  Row,
  Col,
  Button,
} from "reactstrap";
// core components
import HeaderCustom from "components/Headers/HeaderCustom.js";
import DeviceSetting from "./components/DeviceSetting";
const DevicesPage = () => {
  const devices = [
    {
      icon: "bi bi-tv",
      name: "Air Quality Monitor",
      id: "Device" + Math.floor(Math.random() * 16777215).toString(16),
      tooltip_content: "Select device",
    },
    {
      icon: "bi bi-phone",
      name: "Smart Remote",
      id: "Device" + Math.floor(Math.random() * 16777215).toString(16),
      tooltip_content: "Select device",
    },
    {
      icon: "bi bi-lightbulb",
      name: "Smart Light",
      id: "Device" + Math.floor(Math.random() * 16777215).toString(16),
      tooltip_content: "Select device",
    },
    {
      icon: "bi bi-camera-video",
      name: "Camera",
      id: "Device" + Math.floor(Math.random() * 16777215).toString(16),
      tooltip_content: "Select device",
    },
    {
      icon: "ni ni-ui-04",
      name: "Others",
      id: "Device" + Math.floor(Math.random() * 16777215).toString(16),
      tooltip_content: "Select device",
    },
  ];
  const [isShowModal, setIsShowModal] = useState(false);

  const toggleModal = () => {
    setIsShowModal(!isShowModal);
  };
  return (
    <>
      <HeaderCustom />
      <DeviceSetting
        isShowModal={isShowModal}
        setIsShowModal={setIsShowModal}
      ></DeviceSetting>
      {/* Page content */}
      <Container className="mt--7" fluid>
        {/* Table */}
        <Row>
          <div className="col">
            <Card className="shadow">
              <CardHeader className="bg-transparent">
                <h3 className="mb-0">Devices List</h3>
              </CardHeader>
              <CardBody>
                <Row className="icon-examples">
                  {devices.map((device, index) => {
                    return (
                      <Col lg="3" md="6">
                        <Button
                          className="btn-icon-clipboard"
                          data-placement="top"
                          id={device.id}
                          type="button"
                          onClick={toggleModal}
                        >
                          <div>
                            <i className={device.icon} />
                            <span>{device.name}</span>
                          </div>
                        </Button>
                        {/* <UncontrolledTooltip
                          delay={0}
                          trigger="hover"
                          target={device.id}
                        >
                          {device.tooltip_content}
                        </UncontrolledTooltip> */}
                      </Col>
                    );
                  })}
                </Row>
              </CardBody>
            </Card>
          </div>
        </Row>
      </Container>
    </>
  );
};

export default DevicesPage;
