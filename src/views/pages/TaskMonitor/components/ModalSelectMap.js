import React, { useState } from "react";
// reactstrap components
import {
  Modal,
  Row,
  Col,
  ModalHeader,
  ModalBody,
  ModalFooter,
  TabContent,
  TabPane,
} from "reactstrap";
import { getAllTargetPoint } from "network/ApiAxios";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import MenuItem from "@mui/material/MenuItem";
import SendIcon from "@mui/icons-material/Send";
import { ShowToastMessage } from "utils/ShowToastMessage";
import {
  writeStorage,
  deleteFromStorage,
  useLocalStorage,
} from "@rehooks/local-storage";

const ModalSelectMap = (props) => {
  const [mapList, setMapList] = useLocalStorage("mapList", []);
  const [mapInfo, setMapInfo] = useLocalStorage("mapInfo", {
    currentMapId: 1,
    imageMapSource: "",
    mapConfig: {},
  });
  const [mapId, setMapId] = useState(mapInfo.currentMapId || 1);

  const handlehandleModalSelectMap = (e) => {
    const { name, value } = e.target;
    if (name === "mapId") {
      setMapId(value);
    }
    console.log(
      "🚀 ~ file: ModalSelectMap.js:45 ~ handlehandleModalSelectMap ~ e.target:",
      e.target
    );
  };
  return (
    <Modal
      className="modal-dialog-centered"
      size="md"
      isOpen={props.isShowModalSelectMap}
      toggle={() => {
        if (Object.keys(props.allTargetPointList).length > 0) {
          props.setIsShowModalSelectMap(!props.isShowModalSelectMap);
        }
      }}
    >
      <div
        className="pb-2 modal-header"
        style={{
          borderBottom: "2px solid #e0a9a98a ",
        }}
      >
        <h2 className="modal-title" id="exampleModalLabel">
          Select map before create task for Robot
        </h2>
        {/* <button
          aria-label="Close"
          className="close"
          data-dismiss="modal"
          type="button"
          onClick={() => props.setIsShowModalSelectMap(false)}
        >
          <span aria-hidden={true}>X</span>
        </button> */}
      </div>
      <ModalBody className="pt-2 mt-2">
        <Col>
          <Row className="">
            <TextField
              value={mapId ?? ""}
              // defaultValue={"tb3_" + index}
              id={"mapId"}
              name="mapId"
              label="Select map"
              variant="outlined"
              select
              fullWidth
              required
              onChange={(e) => handlehandleModalSelectMap(e)}
            >
              {mapList.map((map, index) => (
                <MenuItem key={index} value={map.id}>
                  {map.mapName}
                </MenuItem>
              ))}
            </TextField>
          </Row>
          <br />
          <Row>
            <Button
              variant="contained"
              endIcon={<SendIcon />}
              color="success"
              fullWidth
              onClick={async () => {
                try {
                  const response = await getAllTargetPoint(mapId);

                  const { data } = response;
                  if (!data.success) {
                    ShowToastMessage({
                      title: "fetchAllTargetPoints",
                      message: "Can not get all target point from db",
                      type: "error",
                    });
                    return;
                  }
                  if (data.GoalPoseArray) {
                    await props?.setAllTargetPointList({
                      ...data.GoalPoseArray,
                    });
                  }
                  props?.setIsShowModalSelectMap(false);
                  ShowToastMessage({
                    title: "fetch data",
                    message: `Fetch all target point from mapId ${mapId}`,
                    type: "success",
                  });
                } catch (error) {
                  console.log(error.message);
                }
              }}
            >
              LOAD
            </Button>
          </Row>
        </Col>
      </ModalBody>
    </Modal>
  );
};
export default ModalSelectMap;
