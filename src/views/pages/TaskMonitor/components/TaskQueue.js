import React, { useState, useContext, useEffect } from "react";
import Stepper from "@mui/material/Stepper";
import Step from "@mui/material/Step";
import Box from "@mui/material/Box";
import StepButton from "@mui/material/StepButton";
import CircularProgress from "@mui/material/CircularProgress";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import { ShowToastMessage } from "../../../../utils/ShowToastMessage";
import {
  getTaskQueueFromAllRobots,
  resetAllTaskQueue,
  resetAllGoalsForOneRobot,
  sendNewTask,
  setStateAccordingToRobotId,
  callServiceActiveGoalAgain,
} from "network/ApiAxios";

const TaskQueue = ({
  taskQueueDetail,
  setAllTaskQueues,
  setStatusOfAllRobots,
}) => {
  // console.log("🚀 ~ file: TaskQueue.js:24 ~ taskQueueDetail:", taskQueueDetail)
  const [ignoreObstacle, setIgnoreObstacle] = React.useState(false);
  const trySetStateAccodingToRobotId = async (robotId) => {
    try {
      const state = true;
      const response = await setStateAccordingToRobotId({ robotId, state });
      console.log("🚀 ~ file: Login.js:47 ~ tryLogin ~ response:", response);
      const { data } = response;
      if (!data.success) {
        return;
      }

      ShowToastMessage({
        title: "trySetStateAccodingToRobotId",
        message: `${robotId} navigate to next point `,
        type: "success",
      });
    } catch (error) {
      console.log("🚀 ~ file: Login.js:61 ~ tryLogin ~ error:", error);
    }
  };

  const tryResetAllGoals = async (robotId) => {
    try {
      const response = await resetAllGoalsForOneRobot(robotId);
      console.log("🚀 ~ file: Login.js:47 ~ tryLogin ~ response:", response);
      const { data } = response;
      if (!data.success) {
        return;
      }

      await setAllTaskQueues((currentAllTaskQueue) => {
        return {
          ...currentAllTaskQueue,
          [data.robotName]: data.taskQueueReset,
        };
      });

      await setStatusOfAllRobots(data["statusOfAllRobots"]);
      ShowToastMessage({
        title: "tryResetAllGoals",
        message: `Reset All goals of ${data.robotName} successfully`,
        type: "success",
      });
    } catch (error) {
      console.log("🚀 ~ file: Login.js:61 ~ tryLogin ~ error:", error);
    }
  };

  const tryActiveGoalAgain = async (robotId, activeGoalAgain) => {
    console.log(
      "🚀 ~ file: TaskQueue.js:72 ~ tryActiveGoalAgain ~ robotId:",
      robotId
    );
    try {
      const response = await callServiceActiveGoalAgain({
        robotId,
        activeGoalAgain,
      });
      const { data } = response;
      if (!data.success) {
        return;
      }
      ShowToastMessage({
        title: "tryActiveGoalAgain",
        message: `Try active current goal of ${data.robotName}`,
        type: "success",
      });
      setIgnoreObstacle(data.message.ignoreObstacle);
      console.log(
        "🚀 ~ file: TaskQueue.js:92 ~ tryActiveGoalAgain ~ data.message:",
        data.message
      );
    } catch (error) {
      console.log("🚀 ~ file: Login.js:61 ~ tryLogin ~ error:", error);
    }
  };

  return (
    <>
      <h3 className="mb-2">
        {" "}
        {`Task Queue ${taskQueueDetail[0].robotName}`}
        {taskQueueDetail[0].taskId &&
          ` ( taskId: ${taskQueueDetail[0].taskId})`}
      </h3>
      <Stepper
        nonLinear
        activeStep={taskQueueDetail[0].indexActiveTask - 1} // alternativeLabel
      >
        {taskQueueDetail.length > 0 &&
          taskQueueDetail
            .filter(function (task) {
              return !task.hasOwnProperty("indexActiveTask");
            })
            .map((task, index) => {
              const isTaskFinish =
                task["isDone"] &&
                index === taskQueueDetail[0].indexActiveTask - 1;
              const isLastTaskFinish =
                task["isDone"] && index === taskQueueDetail.length - 2;

              const labelProps = {};
              if (isTaskFinish && !isLastTaskFinish) {
                labelProps.optional = (
                  <Typography variant="caption" color="green">
                    Click to confirm
                  </Typography>
                );

                // labelProps.error = true;
              }
              return (
                <Step
                  key={index}
                  completed={task["isDone"]}
                  sx={{ position: "relative" }}
                >
                  {!task["isDone"] &&
                    index === taskQueueDetail[0].indexActiveTask - 1 && (
                      <CircularProgress
                        size={32}
                        sx={{
                          color: "green",
                          position: "absolute",
                          top: 0,
                          left: 5,
                          zIndex: 1,
                        }}
                      />
                    )}
                  <StepButton
                    {...labelProps}
                    disabled={isTaskFinish && !isLastTaskFinish ? false : true}
                    color="inherit"
                    onClick={() =>
                      trySetStateAccodingToRobotId(taskQueueDetail[0].robotName)
                    }
                  >
                    <div>{task.targetName}</div>
                    <div>{task.cargoVolume + " kg"}</div>
                  </StepButton>
                </Step>
              );
            })}
      </Stepper>
      <div key="dev-00" className="pl-2">
        {taskQueueDetail.length > 0 &&
        taskQueueDetail[taskQueueDetail.length - 1].statusTask ? (
          <React.Fragment>
            <Typography sx={{ mt: 1, mb: 0 }}>
              All task completed - you&apos;re finished
            </Typography>
          </React.Fragment>
        ) : (
          <React.Fragment>
            <Typography sx={{ mt: 1, mb: 0, py: 1 }}>
              Current target:{" "}
              {taskQueueDetail[taskQueueDetail[0].indexActiveTask] &&
                taskQueueDetail[taskQueueDetail[0].indexActiveTask].targetName}
            </Typography>
          </React.Fragment>
        )}
        <Box sx={{ display: "flex", flexDirection: "row", pt: 0 }}>
          <Box sx={{ flex: "1 1 auto" }} />
          <Button
            onClick={() => {
              tryResetAllGoals(taskQueueDetail[0].robotName);
              // setActiveStepTb0(0);
            }}
          >
            Reset Queue
          </Button>
          {!ignoreObstacle ? (
            <Button
              onClick={() => {
                tryActiveGoalAgain(taskQueueDetail[0].robotName, true);
                // setActiveStepTb0(0);
              }}
            >
              Ignore Obstacle
            </Button>
          ) : (
            <Button
              onClick={() => {
                tryActiveGoalAgain(taskQueueDetail[0].robotName, false);
                // setActiveStepTb0(0);
              }}
            >
              Detect Obstacle
            </Button>
          )}
        </Box>
      </div>
    </>
  );
};
export default TaskQueue;
