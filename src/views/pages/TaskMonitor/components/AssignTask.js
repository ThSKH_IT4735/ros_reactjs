import React, { useState, useContext, useEffect } from "react";
// reactstrap components
import {
  Modal,
  Row,
  Col,
  ModalHeader,
  ModalBody,
  ModalFooter,
  TabContent,
  TabPane,
} from "reactstrap";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import DeleteIcon from "@mui/icons-material/Delete";
import IconButton from "@mui/material/IconButton";
import MenuItem from "@mui/material/MenuItem";
import Checkbox from "@mui/material/Checkbox";
import SendIcon from "@mui/icons-material/Send";
import Tooltip from "@mui/material/Tooltip";
import LibraryAddIcon from "@mui/icons-material/LibraryAdd";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import ArrowForwardIcon from "@mui/icons-material/ArrowForward";
import { useNewTaskInfo } from "../index";
import InputAdornment from "@mui/material/InputAdornment";
import { ShowToastMessage } from "utils/ShowToastMessage";
import { sendNewTask } from "network/ApiAxios";

const AssignTask = (props) => {
  const { newTaskInfo, setNewTaskInfo } = useNewTaskInfo();

  const [isDuplicateTargetName, setIsDeuplicateTargetName] =
    React.useState(false);
  const [activeTab, setActiveTab] = React.useState("1");

  const [isPickRobot, setIsPickRobot] = React.useState(false);

  useEffect(() => {
    console.log("🚀 ~ useEffect ~ newTaskInfo:", newTaskInfo);
  }, [newTaskInfo]);
  const handleTaskNameChange = (e) => {
    const { name, value } = e.target;
    setNewTaskInfo((newTaskInfo) => ({ ...newTaskInfo, taskName: value }));
  };

  const handleTaskDescriptionChange = (e) => {
    const { name, value } = e.target;
    setNewTaskInfo((newTaskInfo) => ({
      ...newTaskInfo,
      taskDescription: value,
    }));
  };

  const handleAssignTask = (e, index) => {
    const { name, value } = e.target;

    const list = [...newTaskInfo.targetPointList];
    // console.log("🚀 ~ file: AssignTask.js:38 ~ handleAssignTask ~ list:", list);
    if (name === "cargoVolume") {
      list[index][name] = +value;
    } else {
      list[index][name] = value;
    }

    setNewTaskInfo((newTaskInfo) => {
      return { ...newTaskInfo, targetPointList: list };
    });
  };

  const handleRemoveTarget = (index) => {
    const list = [...newTaskInfo.targetPointList];
    list.splice(index, 1);

    setNewTaskInfo((newTaskInfo) => {
      return {
        ...newTaskInfo,
        targetPointList: list,
      };
    });
  };

  const handleAddTarget = () => {
    if (
      newTaskInfo.targetPointList.length ===
      Object.keys(props.allTargetPointList).filter((key) => {
        return !key.includes("home") && !key.includes("point");
      }).length
    )
      return;

    setNewTaskInfo((newTaskInfo) => {
      const targetPointList = [
        ...newTaskInfo.targetPointList,
        {
          targetName: newTaskInfo.targetPointList[0].targetName,
          cargoVolume: 20,
          isDone: false,
        },
      ];
      return {
        ...newTaskInfo,
        targetPointList,
      };
    });
  };

  const trySendNewTaskToServer = async (newTask) => {
    try {
      const response = await sendNewTask(newTask);
      console.log("🚀 ~ file: Login.js:47 ~ tryLogin ~ response:", response);
      const { data } = response;
      ShowToastMessage({
        title: "trySendNewTaskToServer",
        message: data.success
          ? `Create new task with handle id ${data.taskId} `
          : data.message,
        type: data.success ? "success" : "error",
      });
      if (!data.success) {
        return;
      }
      props.setIsShowModalAssignTask(false);
    } catch (error) {
      console.log("🚀 ~ file: Login.js:61 ~ tryLogin ~ error:", error);
    }
  };
  const handleCreateTaskList = () => {
    const seen = new Set();
    const hasDuplicates = newTaskInfo.targetPointList.some((obj) => {
      return seen.size === seen.add(obj.targetName).size;
    });

    if (hasDuplicates) {
      setIsDeuplicateTargetName(true);
      return;
    } else {
      setIsDeuplicateTargetName(false);
      // ShowToastMessage({
      //   title: "handleCreateTaskList",
      //   message: "Create new task successfully",
      //   type: "success",
      // });
      trySendNewTaskToServer(newTaskInfo);

      // props.setIsShowModalAssignTask(false);
    }
  };

  return (
    <Modal
      className="modal-dialog-centered"
      size="md"
      isOpen={props.isShowModalAssignTask}
      toggle={() => {
        props.setIsShowModalAssignTask();
      }}
    >
      <div
        className="pb-2 modal-header"
        style={{
          borderBottom: "2px solid #e0a9a98a ",
        }}
      >
        <h2 className="modal-title" id="exampleModalLabel">
          Create New Task
        </h2>
        <button
          aria-label="Close"
          className="close"
          data-dismiss="modal"
          type="button"
          onClick={() => props.setIsShowModalAssignTask(false)}
        >
          <span aria-hidden={true}>X</span>
        </button>
      </div>
      <ModalBody className="pt-2 mt-2">
        <Row className="">
          <Col className="md-6 pl-3 pr-3 ">
            <TabContent activeTab={activeTab}>
              <TabPane tabId={"1"}>
                <Row className="mb-4">
                  <Col>
                    <TextField
                      className="mb-3"
                      id="taskName"
                      value={newTaskInfo.taskName ?? ""}
                      name="taskName"
                      label="Task name"
                      variant="outlined"
                      fullWidth
                      onChange={(e) => handleTaskNameChange(e)}
                    ></TextField>
                    <TextField
                      id="taskName"
                      name="taskName"
                      value={newTaskInfo.taskDescription ?? ""}
                      label="Task description"
                      variant="outlined"
                      fullWidth
                      onChange={(e) => handleTaskDescriptionChange(e)}
                    ></TextField>
                  </Col>
                </Row>
                {/* ___ SubTask list details ___ */}
                <Row className="mb-1 pl-3">
                  <div
                    style={{
                      display: "flex",
                      flex: 1,
                      alignContent: "center",
                      justifyContent: "space-between",
                      alignItems: "center",
                    }}
                  >
                    <h4 className="">___ SubTask list details ___ </h4>

                    <Row className="pl-3 pr-3 mb-3">
                      <IconButton
                        color="info"
                        aria-label="show ros monit or"
                        onClick={handleAddTarget}
                        className="button__background"
                      >
                        <Tooltip title="Add subTask" placement="top" arrow>
                          <LibraryAddIcon />
                        </Tooltip>
                      </IconButton>
                    </Row>
                  </div>
                </Row>
                <Row className="">
                  <Col>
                    {newTaskInfo.targetPointList.map((targetPoint, index) => (
                      <>
                        <Row className="mb-2">
                          <Col className="">
                            <TextField
                              key={index}
                              value={targetPoint.targetName ?? ""}
                              // defaultValue={"tb3_" + index}
                              id={"targetName" + index}
                              name="targetName"
                              label="targetName"
                              variant="outlined"
                              select
                              fullWidth
                              required
                              onChange={(e) => handleAssignTask(e, index)}
                            >
                              {Object.keys(props.allTargetPointList)
                                .filter((targetPoint) => {
                                  return targetPoint.search("home") < 0;
                                })
                                .map((objKey, index) => (
                                  <MenuItem key={index} value={objKey}>
                                    {objKey}
                                  </MenuItem>
                                ))}
                            </TextField>
                          </Col>
                          <Col className="md-6">
                            <TextField
                              key={index}
                              type="number"
                              value={targetPoint.cargoVolume ?? ""}
                              // defaultValue={"tb3_" + index}
                              id={"cargoVolume" + index}
                              name="cargoVolume"
                              label="cargoVolume"
                              variant="outlined"
                              // select
                              fullWidth
                              InputProps={{
                                endAdornment: (
                                  <InputAdornment position="end">
                                    kg
                                  </InputAdornment>
                                ),
                              }}
                              inputProps={{
                                "aria-label": "weight",
                              }}
                              onChange={(e) => handleAssignTask(e, index)}
                            ></TextField>
                          </Col>

                          {newTaskInfo.targetPointList.length !== 1 && (
                            <IconButton
                              color="secondary"
                              aria-label="show ros monit or"
                              onClick={() => handleRemoveTarget(index)}
                            >
                              <Tooltip title="Delete" placement="top" arrow>
                                <DeleteIcon></DeleteIcon>
                              </Tooltip>
                            </IconButton>
                          )}
                        </Row>

                        {newTaskInfo.targetPointList.length - 1 === index && (
                          <>
                            <Row className="pl-3">
                              {!isDuplicateTargetName ? null : (
                                <div className="text-muted font-italic">
                                  <small>
                                    <span
                                      className={`text-red font-weight-700`}
                                    >
                                      Duplicate Target Name
                                    </span>
                                  </small>
                                </div>
                              )}
                            </Row>
                          </>
                        )}
                      </>
                    ))}
                  </Col>
                </Row>
                {/* ___ Advance Setting ___ */}
                <Row className="mb-0 pl-3">
                  <div
                    style={{
                      display: "flex",
                      flex: 1,
                      alignContent: "center",
                      justifyContent: "space-between",
                      alignItems: "center",
                    }}
                  >
                    <h4 className="">___ Advance Setting ___</h4>

                    <IconButton
                      color="secondary"
                      aria-label="show ros monit or"
                      onClick={() => {
                        setActiveTab("2");
                      }}
                      className="button__background"
                    >
                      <ArrowForwardIcon></ArrowForwardIcon>
                    </IconButton>
                  </div>
                </Row>
              </TabPane>
              <TabPane tabId={"2"}>
                {/* ___ Advance Setting ___ */}
                <Row className="mb-0 pl-3 pr-3">
                  <div
                    style={{
                      display: "flex",
                      flex: 1,
                      alignContent: "center",
                      justifyContent: "space-between",
                      alignItems: "center",
                    }}
                  >
                    <h4 className="">___ Advance Setting ___</h4>
                    <IconButton
                      color="secondary"
                      aria-label="show ros monit or"
                      onClick={() => {
                        setActiveTab("1");
                      }}
                      className="button__background"
                    >
                      <ArrowBackIcon></ArrowBackIcon>
                    </IconButton>
                  </div>
                </Row>
                <Row className="mb-3">
                  <Col>
                    <Row className="pl-2">
                      <span className="right">
                        {" "}
                        <Checkbox
                          checked={newTaskInfo.pathOptimization}
                          defaultChecked
                          size="small"
                          onChange={(e) =>
                            setNewTaskInfo((newTaskInfo) => ({
                              ...newTaskInfo,
                              pathOptimization: e.target.checked,
                            }))
                          }
                        />
                        Path Optimization
                      </span>
                    </Row>
                    <Row className="pl-2">
                      <span className="right">
                        {" "}
                        <Checkbox
                          checked={newTaskInfo.autoGoHome}
                          defaultChecked
                          size="small"
                          onChange={(e) =>
                            setNewTaskInfo((newTaskInfo) => ({
                              ...newTaskInfo,
                              autoGoHome: e.target.checked,
                            }))
                          }
                        />
                        Automatically go home
                      </span>
                    </Row>
                    <Row className="pl-2">
                      <span className="right">
                        {" "}
                        <Checkbox
                          checked={isPickRobot}
                          defaultChecked
                          size="small"
                          onChange={(e) => {
                            setIsPickRobot(e.target.checked);
                            setNewTaskInfo((newTaskInfo) => ({
                              ...newTaskInfo,
                              robotWillCall: "",
                            }));
                          }}
                        />
                        Choose a robot to perform the task
                      </span>
                    </Row>
                    <Row className="pl-3 pr-3">
                      <TextField
                        disabled={!isPickRobot}
                        key={2}
                        // defaultValue={"tb3_" + index}
                        id={"robotName"}
                        name="robotName"
                        label="Select Robot"
                        variant="outlined"
                        select
                        fullWidth
                        value={newTaskInfo.robotWillCall ?? ""}
                        onChange={(e) =>
                          setNewTaskInfo((newTaskInfo) => ({
                            ...newTaskInfo,
                            robotWillCall: e.target.value,
                          }))
                        }
                      >
                        {Object.keys(props.statusOfAllRobots).length > 0
                          ? Object.keys(props.statusOfAllRobots).map(
                              (objKey, index) => (
                                <MenuItem key={index} value={objKey}>
                                  {objKey}
                                </MenuItem>
                              )
                            )
                          : null}
                      </TextField>
                    </Row>
                  </Col>
                </Row>
              </TabPane>
            </TabContent>
          </Col>
        </Row>
        <Row className="pl-3 pr-3">
          <Button
            variant="contained"
            endIcon={<SendIcon />}
            color="success"
            fullWidth
            onClick={() => {
              handleCreateTaskList();
            }}
          >
            Create Task
          </Button>
        </Row>
      </ModalBody>
    </Modal>
  );
};
export default AssignTask;
