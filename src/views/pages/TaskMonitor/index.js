import React, { useState, createContext, useContext, useEffect } from "react";
import { Container, Card, Row, Col, CardHeader, CardBody } from "reactstrap";
import {
  writeStorage,
  deleteFromStorage,
  useLocalStorage,
} from "@rehooks/local-storage";

import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Tooltip from "@mui/material/Tooltip";
import { IconButton } from "@mui/material";
import CircularProgress from "@mui/material/CircularProgress";
import AddTaskIcon from "@mui/icons-material/AddTask";
import MapsHomeWorkIcon from "@mui/icons-material/MapsHomeWork";
import DisplaySettingsIcon from "@mui/icons-material/DisplaySettings";

import AssignTask from "./components/AssignTask";
import ModalSelectMap from "./components/ModalSelectMap";
import TaskQueue from "./components/TaskQueue";
import HeaderCustom from "../../../components/Headers/HeaderCustom.js";
import socket from "../../../socket";
// import show toast message
import { ShowToastMessage } from "../../../utils/ShowToastMessage";
// import network
import {
  getTaskQueueFromAllRobots,
  resetAllTaskQueue,
  resetAllGoalsForOneRobot,
  sendNewTask,
  setStateAccordingToRobotId,
} from "network/ApiAxios";
import configs from "configs";

export const TaskContext = createContext();

export default function HorizontalNonLinearStepper() {
  // check if connect to webserver
  const [isSocketConnected, setIsSocketConnected] = useState(socket.connected);

  // const [allTaskQueues, setAllTaskQueues] = useLocalStorage("allTaskQueues", {});
  const [allTaskQueues, setAllTaskQueues] = useState({});
  const [statusOfAllRobots, setStatusOfAllRobots] = useLocalStorage(
    "statusOfAllRobots",
    {}
  );
  const [allTargetPointList, setAllTargetPointList] = useLocalStorage(
    "allTargetPointList",
    {}
  );

  const [isSendTaskQueueToRobot, setIsSendTaskQueueToRobot] = useState(false);
  const [isShowModalAssignTask, setIsShowModalAssignTask] = useState(false);
  const [isShowModalSelectMap, setIsShowModalSelectMap] = useState(true);
  const [newTaskInfo, setNewTaskInfo] = useState({
    taskName: "default_task",
    taskDescription: "new_task",
    pathOptimization: true,
    autoGoHome: true,
    robotWillCall: "",
    targetPointList: [
      { targetName: "point_1", cargoVolume: 20, isDone: false },
    ],
  });

  // const trySendNewTaskToServer = async (newTask) => {
  //   try {
  //     const response = await sendNewTask(newTask);
  //     console.log("🚀 ~ file: Login.js:47 ~ tryLogin ~ response:", response);
  //     const { data } = response;
  //     ShowToastMessage({
  //       title: "trySendNewTaskToServer",
  //       message: data.success
  //         ? `Create new task with id ${data.taskId} `
  //         : data.message,
  //       type: data.success ? "success" : "error",
  //     });
  //     if (!data.success) {
  //       return;
  //     }
  //   } catch (error) {
  //     console.log("🚀 ~ file: Login.js:61 ~ tryLogin ~ error:", error);
  //   }
  //   setIsSendTaskQueueToRobot(true);
  // };

  const tryResetAllTaskQueue = async () => {
    try {
      const response = await resetAllTaskQueue();
      console.log("🚀 ~ file: Login.js:47 ~ tryLogin ~ response:", response);
      const { data } = response;
      if (!data.success) {
        return;
      }
      await setAllTaskQueues(data.allTaskQueues);

      await setStatusOfAllRobots(data["statusOfAllRobots"]);

      ShowToastMessage({
        title: "tryResetAllTaskQueue",
        message: data.message,
        type: "success",
      });
    } catch (error) {
      console.log("🚀 ~ file: Login.js:61 ~ tryLogin ~ error:", error);
    }
  };

  // connect to socket server
  async function onEventStatusOfAllRobots(response) {
    console.log("🚀 ~~ onEventStatusOfAllRobots ~ response:", response);
    setStatusOfAllRobots(response);
  }

  // fetch data
  useEffect(() => {
    const fetchAllTaskQueue = async () => {
      try {
        const response = await getTaskQueueFromAllRobots();

        const { data } = response;
        console.log(
          "🚀 ~ file: index.js:160 ~ fetchAllTaskQueue ~ data:",
          data
        );
        if (!data.success) {
          ShowToastMessage({
            title: "fetchAllTaskQueue",
            message: "Can not get task queue",
            type: "error",
          });
          return;
        }
        await setAllTaskQueues(data.allTaskQueues);

        await setStatusOfAllRobots(data["statusOfAllRobots"]);

        setIsSendTaskQueueToRobot(true);
        ShowToastMessage({
          title: "fetch data",
          message: data.message,
          type: "success",
        });
      } catch (error) {
        console.log(
          "🚀 ~ file: index.js:223 ~ fetchAllTaskQueue ~ error:",
          error
        );
      }
    };

    fetchAllTaskQueue();
  }, []);

  // connect to socket server
  useEffect(() => {
    function onConnect() {
      console.log("🚀 ~ file: index.js:87 ~ onConnect ~ onConnect:");
      setIsSocketConnected(true);
    }

    function onDisconnect() {
      setIsSocketConnected(false);
    }

    async function onEventUpdateTaskQueue(response) {
      const { robotName, taskQueueUpdate } = response;
      // const allTaskQueuesUpdate = {};
      // Object.keys(allTaskQueues).forEach((key, index) => {
      //   if (key === robotName) {
      //     allTaskQueuesUpdate[key] = taskQueueUpdate;
      //   } else {
      //     allTaskQueuesUpdate[key] = allTaskQueues[key];
      //   }
      // });

      await setAllTaskQueues((currentAllTaskQueue) => {
        return {
          ...currentAllTaskQueue,
          [robotName]: taskQueueUpdate,
        };
      });
    }

    async function onEventWorkerNotify(response) {
      if (response.type === "AssignTask")
        ShowToastMessage({
          title: "onEventWorkerNotify",
          message: response.message,
          type: response.success ? "info" : "warning",
        });
    }

    socket.connect();
    socket.on("connect", onConnect);
    socket.on("disconnect", onDisconnect);
    socket.on(`statusOfAllRobots`, onEventStatusOfAllRobots);
    socket.on("updateTaskQueue", onEventUpdateTaskQueue);
    socket.on(`WorkerNotify`, onEventWorkerNotify);

    return () => {
      socket.off("connect", onConnect);
      socket.off("disconnect", onDisconnect);
      socket.off(`statusOfAllRobots`, onEventStatusOfAllRobots);
      socket.off("updateTaskQueue", onEventUpdateTaskQueue);
      socket.off(`WorkerNotify`, onEventWorkerNotify);
    };
  }, []);

  const handleCreateTaskList = async () => {
    console.log("btn click");
    // setIsSendTaskQueueToRobot(false);
    setIsShowModalAssignTask(true);
  };

  const showTaskQueueDetails = () => {
    console.log("showTaskQueueDetails");

    window.open(
      `${configs.DOMAIN_SERVER}/queueDashboard`,
      "_blank",
      "noopener,noreferrer"
    );
  };

  return (
    <TaskContext.Provider value={{ newTaskInfo, setNewTaskInfo }}>
      <HeaderCustom />
      <AssignTask
        allTargetPointList={allTargetPointList}
        statusOfAllRobots={statusOfAllRobots}
        isShowModalAssignTask={isShowModalAssignTask}
        setIsShowModalAssignTask={setIsShowModalAssignTask}
      ></AssignTask>
      <ModalSelectMap
        isShowModalSelectMap={isShowModalSelectMap}
        setIsShowModalSelectMap={setIsShowModalSelectMap}
        setAllTargetPointList={setAllTargetPointList}
        allTargetPointList={allTargetPointList}
      ></ModalSelectMap>
      <Container className="mt--7" fluid>
        <Card>
          <CardHeader
            className="bg-transparent"
            style={{ position: "relative" }}
          >
            <div
              className="row"
              style={{
                padding: "0rem 1.5rem",
                alignItems: "center",
              }}
            >
              <h3 className="mb-0">Create Task:</h3>
              {/* Create new task */}
              <Tooltip title="Add new task" placement="top" arrow>
                <IconButton
                  style={{
                    position: "absolute",

                    transform: "translate(200px,0)",
                  }}
                  className="button__background"
                  color="success"
                  aria-label="add an alarm"
                  disabled={isShowModalSelectMap}
                  onClick={() => handleCreateTaskList()}
                >
                  <AddTaskIcon />
                </IconButton>
              </Tooltip>

              {/* send new task */}
              <Tooltip title="Select map" placement="top" arrow>
                <IconButton
                  className="button__background"
                  style={{
                    position: "absolute",

                    transform: "translate(300px,0)",
                  }}
                  // disabled={targetPointList.length === 1 ? true: false}
                  color="success"
                  aria-label="add an alarm"
                  onClick={() => setIsShowModalSelectMap(true)}
                >
                  <MapsHomeWorkIcon />
                </IconButton>
              </Tooltip>
              {/* cancel all goals */}
              <Tooltip title="Show task queue details" placement="top" arrow>
                <IconButton
                  style={{
                    position: "absolute",
                    right: "20px",
                  }}
                  className="button__background"
                  color={"success"}
                  aria-label="add an alarm"
                  onClick={showTaskQueueDetails}
                >
                  <DisplaySettingsIcon />
                </IconButton>
              </Tooltip>
            </div>
          </CardHeader>
          <CardBody>
            <Row
              style={{
                padding: "0rem 1rem",
              }}
            >
              {Object.keys(statusOfAllRobots).length === 0 ? (
                <Col>All Robots are not available</Col>
              ) : (
                Object.keys(statusOfAllRobots).map((key, index) => {
                  return (
                    <Col>
                      {`${key} status: `}
                      <span style={{ color: "orange", fontWeight: "bold" }}>
                        {statusOfAllRobots[key] && statusOfAllRobots[key]}
                      </span>
                    </Col>
                  );
                })
              )}
              {/* <Col>
                tb3_0 status:{" "}
                <span style={{ color: "orange", fontWeight: "bold" }}>
                  {statusOfAllRobots["tb3_0"] && statusOfAllRobots["tb3_0"]}
                </span>
              </Col>
              <Col>
                tb3_1 status:{" "}
                <span style={{ color: "orange", fontWeight: "bold" }}>
                  {statusOfAllRobots["tb3_1"] && statusOfAllRobots["tb3_1"]}
                </span>
              </Col>
              <Col>
                tb3_2 status:{" "}
                <span style={{ color: "orange", fontWeight: "bold" }}>
                  {statusOfAllRobots["tb3_2"] && statusOfAllRobots["tb3_2"]}
                </span>
              </Col> */}
            </Row>
          </CardBody>
        </Card>
        <Card className="shadow mt-3">
          <CardHeader className="bg-transparent">
            <div
              className="row"
              style={{
                padding: "0rem 1.5rem",
                alignItems: "center",
              }}
            >
              <h3 className="mb-0"> Task Queue Monitor</h3>
              <div
                style={{
                  flex: 1,
                  display: "flex",
                  justifyContent: "flex-end",
                }}
              >
                {isSocketConnected ? (
                  <h4 className="text-green mb-0">Socket connected</h4>
                ) : (
                  <h4 className="text-red mb-0">Socket disconnected</h4>
                )}
              </div>
            </div>
          </CardHeader>
          <CardBody>
            {!isSendTaskQueueToRobot ? (
              <div>
                <CircularProgress />
                <h4> Wait to get new data </h4>
              </div>
            ) : (
              <>
                {Object.keys(allTaskQueues).length > 0 &&
                  Object.keys(allTaskQueues).map((key, index) => (
                    <div
                      key={`div_${index}`}
                      className="container-task-monitor"
                    >
                      <div className=" bg-transparent">
                        <TaskQueue
                          taskQueueDetail={allTaskQueues[key]}
                          setAllTaskQueues={setAllTaskQueues}
                          setStatusOfAllRobots={setStatusOfAllRobots}
                        ></TaskQueue>
                      </div>
                    </div>
                  ))}
                {/* <div key="div-0" className="container-task-monitor">
                  <div className=" bg-transparent">
                    <TaskQueue
                      taskQueue={taskQueueTb0}
                      setTaskQueue={setTaskQueueTb0}
                      setStatusOfAllRobots={setStatusOfAllRobots}
                    ></TaskQueue>
                  </div>
                </div>
                <div key="div-1" className="container-task-monitor">
                  <div className=" bg-transparent">
                    <TaskQueue
                      taskQueue={taskQueueTb1}
                      setTaskQueue={setTaskQueueTb1}
                      setStatusOfAllRobots={setStatusOfAllRobots}
                    ></TaskQueue>
                  </div>
                </div>
                <div key="div-2" className="container-task-monitor">
                  <div className=" bg-transparent">
                    <TaskQueue
                      taskQueue={taskQueueTb2}
                      setTaskQueue={setTaskQueueTb2}
                      setStatusOfAllRobots={setStatusOfAllRobots}
                    ></TaskQueue>
                  </div>
                </div> */}
              </>
            )}
          </CardBody>
        </Card>
        <Box sx={{ display: "flex", flexDirection: "row" }}>
          <Button color="inherit" onClick={tryResetAllTaskQueue} sx={{ mr: 1 }}>
            Reset All tasks
          </Button>
        </Box>
      </Container>
    </TaskContext.Provider>
  );
}

export function useNewTaskInfo() {
  return {
    newTaskInfo: useContext(TaskContext).newTaskInfo,
    setNewTaskInfo: useContext(TaskContext).setNewTaskInfo,
  };
}
// export let AllTargetPointList = {};
