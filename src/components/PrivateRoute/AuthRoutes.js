import React from "react";
import { Navigate } from "react-router-dom";

const AuthRoutes = ({ children }) => {
  if (localStorage.getItem("accessToken")) {
    // Redirect them to the /login page, but save the current location they were
    // trying to go to when they were redirected. This allows us to send them
    // along to that page after they login, which is a nicer user experience
    // than dropping them off on the home page.

    return <Navigate to="/admin/robot-management" />;
  }

  return children;
};

export default AuthRoutes;
